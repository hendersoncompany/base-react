import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';

import Button from '@mui/material/Button';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Box from '@mui/material/Box';

import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';

class VersionTable extends React.Component {
  constructor(props) {
    super(props);
    if (this.props.type == 0) {
      this.rows = [
        this.createData('6.4.99', true, false, false, '다이나트레이스'),
        this.createData('6.4.8', true, false, true, '6.4.8 릴리즈 버전'),
        this.createData('6.4.7', false, false, false, '6.4.7 릴리즈 버전'),
        this.createData('6.4.6', false, true, false, '6.4.6 릴리즈 버전'),
        this.createData('6.4.5', true, false, true, '6.4.5 릴리즈 버전'),
        this.createData('6.4.4', true, true, true, '6.4.4 릴리즈 버전'),
      ];
    } else {
      this.rows = [
        this.createData('6.4.99', false, true, true, '다이나트레이스'),
        this.createData('6.4.8', false, true, false, '6.4.8 릴리즈 버전'),
        this.createData('6.4.7', true, false, false, '6.4.7 릴리즈 버전'),
        this.createData('6.4.6', true, false, true, '6.4.6 릴리즈 버전'),
        this.createData('6.4.5', false, false, false, '6.4.5 릴리즈 버전'),
        this.createData('6.4.4', false, false, true, '6.4.4 릴리즈 버전'),
      ];
    }
  }
  
  createData(version, release, debug, development, memo) {
    return { version, release, debug, development, memo };
  }

  createButton(has) {
    if(has) {
      return (
        <Button variant="outlined">설치</Button>
      )
    }
  }

  render() {
    return (
      <TableContainer>
          <Table sx={{ minWidth: 650 }}>
            <TableHead>
              <TableRow>
                <TableCell>버전 정보</TableCell>
                <TableCell align="center">운영</TableCell>
                <TableCell align="center">디버그</TableCell>
                <TableCell align="center">개발</TableCell>
                <TableCell>메모</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.rows.map((row) => (
                <TableRow key={row.name}>
                  <TableCell>{row.version}</TableCell>
                  <TableCell align="center">{this.createButton(row.release)}</TableCell>
                  <TableCell align="center">{this.createButton(row.debug)}</TableCell>
                  <TableCell align="center">{this.createButton(row.development)}</TableCell>
                  <TableCell>{row.memo}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
    )
  };
}

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
    >
      {value === index && (
        <VersionTable type={value} />
      )}
    </div>
  );
}

function OSTab() {
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <Box sx={{ width: '100%' }}>
      <Box sx={{ borderBottom: 2, borderColor: 'divider' }}>
        <Tabs value={value} onChange={handleChange}>
          <Tab label="Android"/>
          <Tab label="iOS"/>
        </Tabs>
      </Box>
      <TabPanel value={value} index={0}>
        Anroid
      </TabPanel>
      <TabPanel value={value} index={1}>
        iOS
      </TabPanel>
    </Box>
  );
}

function Storage() {
  return (
    <div className="title">
      <div> 앱 저장소 </div>
      <OSTab />
    </div>
  );
}

export default Storage;
