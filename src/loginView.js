import React from 'react';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Axios from 'axios'

function clickLogin() {
	Axios.get("http://127.0.0.1:8000/", {})
	.then((res) => {
       const { data } = res;
		// this.setState({
		// 	boardList: data,
		// });
		console.log(data[0]["id"]);
        alert("등록 완료!" + data[0]["id"]);
    })
    .catch((e) => {
		console.error(e);
    });
}

function Body() {
	return (
		<Box sx={{ width: '100%' }}>
			<TextField sx={{ width: '100%' }} id="login" label="아이디" variant="outlined" />
			<TextField style={{marginTop: 16}} sx={{ width: '100%' }} useRef="myField" label="패스워드" variant="outlined" />
			<Button style={{marginTop: 16}} onClick={clickLogin} variant="outlined">로그인</Button>
		</Box>
	);
}

function LoginView() {
	return (
		<div className="title">
			<div> 로그인 </div>
			<Body />
		</div>
	);
}

export default LoginView;