import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import LoginView from './loginView.js'
import Storage from './storageView.js'


function Main() {
  return (
    <div className="title">
      <LoginView />
      <Storage />
    </div>
  );
}

// ========================================

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(<Main />);
